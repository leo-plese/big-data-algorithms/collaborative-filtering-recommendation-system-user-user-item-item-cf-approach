from sys import stdin
import numpy as np
from decimal import Decimal, ROUND_HALF_UP

def cf(tbl, queries_list):
    new_tbl_orig = np.copy(tbl)
    new_tbl_orig = np.where(new_tbl_orig == 0, np.nan, new_tbl_orig)

    #############
    new_tbl_t0 = np.copy(new_tbl_orig)
    new_tbl_t0 -= np.nanmean(new_tbl_t0, axis=1).reshape(-1, 1)

    new_tbl_t1 = np.copy(new_tbl_orig)
    new_tbl_t1 -= np.nanmean(new_tbl_t1, axis=0)

    new_tbl_t0[np.isnan(new_tbl_t0)] = 0
    new_tbl_t1[np.isnan(new_tbl_t1)] = 0

    norms_t0 = np.linalg.norm(new_tbl_t0, axis=1)
    norms_t1 = np.linalg.norm(new_tbl_t1, axis=0)

    new_tbl_t0_trans = new_tbl_t0.T

    for q in queries_list:
        I, J, T, K = q
        I -= 1
        J -= 1

        if T == 0:
            item_dot_arr = new_tbl_t0[I,:] @ new_tbl_t0_trans  # 1 x N
            item_norm = norms_t0[I]
            item_mul_norm_arr = item_norm * norms_t0
            item_sim_arr = item_dot_arr / item_mul_norm_arr
            take_item_inds = np.argsort(item_sim_arr)[::-1][1:]
            take_sim_vals = item_sim_arr[take_item_inds]

            take_item_inds = take_item_inds[take_sim_vals>0]
            take_sim_vals = take_sim_vals[take_sim_vals>0]

            tbl_taken_vals = tbl[take_item_inds, J]
            tbl_taken_inds = tbl_taken_vals.nonzero()[0][:K]
            tbl_taken_vals = tbl_taken_vals[tbl_taken_inds]
            take_sim_vals = take_sim_vals[tbl_taken_inds]

            print(Decimal(Decimal(np.sum(tbl_taken_vals * take_sim_vals) / (np.sum(take_sim_vals))).quantize(Decimal('.001'), rounding=ROUND_HALF_UP)))

        else:
            user_dot_arr = new_tbl_t1[:, J] @ new_tbl_t1  # 1 x M
            user_norm = norms_t1[J]
            user_mul_norm_arr = user_norm * norms_t1
            user_sim_arr = user_dot_arr / user_mul_norm_arr
            take_user_inds = np.argsort(user_sim_arr)[::-1][1:]
            take_sim_vals = user_sim_arr[take_user_inds]

            take_user_inds = take_user_inds[take_sim_vals>0]
            take_sim_vals = take_sim_vals[take_sim_vals>0]

            tbl_taken_vals = tbl[I, take_user_inds]
            tbl_taken_inds = tbl_taken_vals.nonzero()[0][:K]
            tbl_taken_vals = tbl_taken_vals[tbl_taken_inds]
            take_sim_vals = take_sim_vals[tbl_taken_inds]

            print(Decimal(Decimal(np.sum(tbl_taken_vals * take_sim_vals) / (np.sum(take_sim_vals))).quantize(Decimal('.001'), rounding=ROUND_HALF_UP)))





if __name__ == "__main__":
    lines = stdin.readlines()

    N, M = tuple(map(int, lines[0].strip().split()))
    to_ind0 = N + 1
    tbl = np.array([list(map(int, lines[i].strip().replace("X", "0").split())) for i in range(1, to_ind0)])

    Q = int(lines[N+1].strip())
    from_ind, to_ind = N+2, N+2+Q
    queries_list = [tuple(map(int, lines[i].strip().split())) for i in range(from_ind, to_ind)]

    cf(tbl, queries_list)




