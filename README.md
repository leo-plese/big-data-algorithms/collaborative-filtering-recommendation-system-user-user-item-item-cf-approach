# Collaborative Filtering Recommendation System User-User Item-Item CF Approach

Implemented in Python.

My lab assignment in Analysis of Massive Datasets, FER, Zagreb.

Task descriptions in "TaskSpecification.pdf".

Created: 2021